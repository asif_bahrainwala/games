asset from:
https://www.pexels.com/photo/planet-earth-220201/
usage under: https://www.pexels.com/creative-commons-images/
Use all photos for free for commercial and noncommercial purposes.

Music from:        https://pixabay.com/music/beautiful-plays-price-of-freedom-33106/
sound effect from: https://mixkit.co/free-sound-effects/space-shooter/

tested on Linux Mint 20
requires:
sudo apt-get install libsfml-dev

use mouse to select multiple tiles
use Z,X,P,L,A,S keys to play around with the tiles
