#include <iostream>
#include <sstream>
#include <memory>
#include <thread>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
using namespace std;

#define NUMBEROFTILESX 10
#define NUMBEROFTILESY 10

float g_expand=1;

float length2(sf::Vector2f f)
{
    return f.x*f.x+f.y*f.y;
}

struct Tile
{
    shared_ptr<sf::Sprite>  sprite_=make_shared<sf::Sprite>();  //Do not use objects as their copying could be problematic
    shared_ptr<sf::Texture> texture_;                           //Do not use objects as their copying could be problematic
    shared_ptr<sf::Sound>   sound_;

    int x_,y_;  //these maintain original positions (and are not updated), they are used for initial texture setting
    string name_;
    sf::Vector2f dest_;
    int sizex_=100,sizey_=100;
    Tile(int x,int y,shared_ptr<sf::Texture> tex,string name,shared_ptr<sf::Sound> sound):sound_(sound),name_(name),texture_(tex),x_(x),y_(y) //position on the grid
    {
        sprite_->setTexture(*texture_);
        sprite_->setTextureRect(sf::IntRect(x_*sizex_*g_expand,y_*sizey_*g_expand,sizex_*g_expand,sizey_*g_expand));
    }

    void TextureStretch()
    {
        sprite_->setTextureRect(sf::IntRect(x_*sizex_*g_expand,y_*sizey_*g_expand,sizex_*g_expand,sizey_*g_expand));
        sprite_->setScale(98.0/sprite_->getTextureRect().width,98.0/sprite_->getTextureRect().height);
    }

    bool Selected_=false;
    bool movement_=false; //set to true if animation is in progress
    int movementcounter_=0;  //if the counter =200, I will just skip the animation

    void MoveTilePixel()
    {
        auto pos=sprite_->getPosition();
        if(length2(dest_-pos)>.1)
        {
            if(sound_->getStatus()!=sound_->Playing)
            {
                sound_->play();
            }

            sf::Vector2f vec=dest_-pos;
            vec.x/=5;
            vec.y/=5;
            sprite_->setPosition(pos+vec);
            movement_=true;
            movementcounter_++;
        }
        else
        {
            movement_=false;
        }

        if(movementcounter_>200)
        {
            movementcounter_=0;
            sprite_->setPosition(dest_);
        }

    }
};


void placeTiles(vector<shared_ptr<Tile>> &tiles)
{
    for(auto &t:tiles)
    {
        t->sprite_->setPosition(t->x_ * t->sizex_,
                                t->y_ * t->sizey_);
        t->dest_=t->sprite_->getPosition();
    }
}

shared_ptr<Tile> GetTileSelected(int x,int y,const vector<shared_ptr<Tile>> &tiles)
{
    for(auto &t:tiles)
    {
        auto pos=t->sprite_->getPosition();
        if(x>pos.x && y>pos.y && x<pos.x+t->sizex_ && y<pos.y+t->sizey_)
            return t;
    }
}

//if any tile is moving, it returns true.
//if no tile is moving, it return false
bool checkforMovement(vector<shared_ptr<Tile>> &tiles)
{
    for(auto &t:tiles)
    {
        if(t->movement_==true)
            return true;
    }
    return false;
}

void MoveTilesUp(vector<shared_ptr<Tile>> &tiles,int x,bool up=true)
{
    if(false==checkforMovement(tiles))
    for(auto &t:tiles)
        if(abs(t->sprite_->getPosition().x-x)<5)
        {
            auto pos=t->sprite_->getPosition();
            if(up)
                pos.y-=t->sizey_;
            else
                pos.y+=t->sizey_;

            t->dest_=pos;
        }
}

void MoveTilesRight(vector<shared_ptr<Tile>> &tiles,int y,bool right=true)
{
    if(false==checkforMovement(tiles))
    for(auto &t:tiles)
    {
        if(abs(t->sprite_->getPosition().y-y)<5)
        {
            auto pos=t->sprite_->getPosition();
            if(right)
                pos.x+=t->sizex_;
            else
                pos.x-=t->sizex_;
            t->dest_=pos;
        }
    }
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(1200, 1200), "2D tiles");

    sf::Music music;
    music.openFromFile("../Tile2D/nelyma-acoustic-version-14368.ogg");
    music.play();
    music.setLoop(true);

    shared_ptr<sf::Sound>       jumpsound=make_shared<sf::Sound>();
    shared_ptr<sf::SoundBuffer> jumpSoundBuffer=make_shared<sf::SoundBuffer>();
    jumpSoundBuffer->loadFromFile("../Tile2D/mixkit-space-coin-win-notification-271.wav"); //load this only once
    jumpsound->setBuffer(*jumpSoundBuffer);

    shared_ptr<sf::Texture> texture=make_shared<sf::Texture>();;
    texture->loadFromFile("../Tile2D/a.jpg");texture->setRepeated(true);texture->setSrgb(true);texture->setSmooth(true);

    vector<shared_ptr<Tile>> tiles;
    //create the tiles
    for(int i=0;i<10;++i)
        for(int j=0;j<10;++j)
        {
            stringstream ss;
            ss<<i<<" "<<j;
            tiles.push_back(make_shared<Tile>(i,j,texture,ss.str(),jumpsound));
        }

    placeTiles(tiles);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();

            if (event.type == sf::Event::KeyPressed)
            {
                cout<<"key press:"<<event.key.code<<endl<<flush;

                if(18 == event.key.code)
                    g_expand++;
                else if(0 == event.key.code)
                    g_expand--;
                else if(15 == event.key.code || 11 == event.key.code)  //for P,S
                {
                    //move all tiles vertically
                    set<int> s;
                    for(auto t:tiles)
                    {
                        if(t->Selected_)
                            s.insert(t->sprite_->getPosition().x);
                    }

                    //we must now move all selected tiles up by 1 unit
                    for(int i:s)
                    {
                        if(15 == event.key.code)
                           MoveTilesUp(tiles,i,true);
                        if(11 == event.key.code)
                           MoveTilesUp(tiles,i,false);
                    }
                }
                else if(23 == event.key.code || 25 == event.key.code)  //for X,Z
                {
                    //move all tiles horizontally
                    set<int> s;
                    for(auto t:tiles)
                    {
                        if(t->Selected_)
                            s.insert(t->sprite_->getPosition().y);
                    }

                    //we must now move all selected tiles right by 1 unit
                    for(int i:s)
                    {
                        if(23 == event.key.code)
                            MoveTilesRight(tiles,i,true);
                        if(25 == event.key.code)
                            MoveTilesRight(tiles,i,false);
                    }
                }
                g_expand=max(1.0f,g_expand);
            }
            if (event.type == sf::Event::MouseButtonPressed)
            {
                cout<<event.mouseButton.x<<" "<<event.mouseButton.y<<endl<<flush;
                auto t=GetTileSelected(event.mouseButton.x,event.mouseButton.y,tiles);

                t->Selected_  = !t->Selected_;
                cout<<t->name_<<flush<<endl;
            }
        }

        window.clear(sf::Color::Blue);

        for(auto &y:tiles)
        {
            if(y->Selected_)
                y->sprite_->setColor(sf::Color(0x80,0x80,0x80));
            else
                y->sprite_->setColor(sf::Color(0xff,0xff,0xff));

            y->TextureStretch();
            y->MoveTilePixel();
            window.draw(*y->sprite_);
        }

        window.display();
    }
    return 0;
}
